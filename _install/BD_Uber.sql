-- MySQL dump 10.13  Distrib 5.7.17, for macos10.12 (x86_64)
--
-- Host: 127.0.0.1    Database: BD_Uber
-- ------------------------------------------------------
-- Server version	5.7.32

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `Ub_menu`
--

DROP TABLE IF EXISTS `Ub_menu`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Ub_menu` (
  `menuid` int(10) NOT NULL AUTO_INCREMENT,
  `menu` varchar(50) COLLATE latin1_general_ci NOT NULL,
  `icono` varchar(40) COLLATE latin1_general_ci NOT NULL,
  `vista` varchar(100) COLLATE latin1_general_ci NOT NULL,
  PRIMARY KEY (`menuid`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Ub_menu`
--

LOCK TABLES `Ub_menu` WRITE;
/*!40000 ALTER TABLE `Ub_menu` DISABLE KEYS */;
INSERT INTO `Ub_menu` VALUES (1,'Conductor','fa-automobile','#'),(2,'Usuario','fa-user','#');
/*!40000 ALTER TABLE `Ub_menu` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Ub_servicios`
--

DROP TABLE IF EXISTS `Ub_servicios`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Ub_servicios` (
  `idservicios` bigint(15) NOT NULL AUTO_INCREMENT,
  `id_usuario` varchar(45) NOT NULL,
  `id_conductor` varchar(45) DEFAULT NULL,
  `ubicacion_usuario` varchar(45) NOT NULL,
  `direccion` varchar(45) NOT NULL,
  `stadoid` varchar(45) NOT NULL,
  PRIMARY KEY (`idservicios`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Ub_servicios`
--

LOCK TABLES `Ub_servicios` WRITE;
/*!40000 ALTER TABLE `Ub_servicios` DISABLE KEYS */;
INSERT INTO `Ub_servicios` VALUES (1,'2','1','4.612959 -74.159988','pescaito','2');
/*!40000 ALTER TABLE `Ub_servicios` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Ub_usuarios`
--

DROP TABLE IF EXISTS `Ub_usuarios`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Ub_usuarios` (
  `usuarioid` bigint(15) NOT NULL AUTO_INCREMENT,
  `nombres` varchar(255) COLLATE latin1_general_ci NOT NULL,
  `contrasena` varchar(255) COLLATE latin1_general_ci NOT NULL,
  `fotografia` varchar(255) COLLATE latin1_general_ci NOT NULL,
  `telefono` varchar(20) COLLATE latin1_general_ci NOT NULL,
  `placa` varchar(45) COLLATE latin1_general_ci NOT NULL,
  `conductor` int(1) NOT NULL,
  `estadoid` int(1) NOT NULL,
  PRIMARY KEY (`usuarioid`),
  KEY `estadoid` (`estadoid`),
  KEY `telefono` (`telefono`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Ub_usuarios`
--

LOCK TABLES `Ub_usuarios` WRITE;
/*!40000 ALTER TABLE `Ub_usuarios` DISABLE KEYS */;
INSERT INTO `Ub_usuarios` VALUES (1,'Juan','81dc9bdb52d04dc20036dbd8313ed055','default.png','3157771111','dgd35B',1,1),(2,'Valentina','81dc9bdb52d04dc20036dbd8313ed055','default.png','1234','',2,1);
/*!40000 ALTER TABLE `Ub_usuarios` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2021-06-12 15:13:34
