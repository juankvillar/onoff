<?php

use Firebase\JWT\JWT;

class ModelSesion
{

    private static $secret_key = 'Sdw1s9x8@';
    private static $encrypt = ['HS256'];
    private static $aud = null;
    /**
     * @param object $db A PDO database connection
     */
    function __construct($db)
    {
        try {
            $this->db = $db;
        } catch (PDOException $e) {
            exit('Database connection could not be established.');
        }
    }

    public function loguearse($usuario, $contrasena)
    {
        $sql = "SELECT * FROM Ub_usuarios where tipoidentificacionid = '$usuario' and contrasena = '$contrasena' and estadoid = 1";
        $query = $this->db->query($sql);
        $data = $query->fetch();

        $res = array();
        if (!$data) {
            $res['error'] = true;
            $res['msj'] = 'Usuario o contraseña invalidos';
        } else {
            $data->fotografia = 'public/dist/img/usuarios/' . $data->fotografia;
            $time = time();
            $token = array(
                'iat' => $time, // Tiempo que inició el token
                //'exp' => $time + (180*60), // Tiempo que expirará el token (+1 hora)
                'aud' => self::Aud(),
                'data' => $data // datos de usuario
            );
            $token = JWT::encode($token, self::$secret_key);
            //return JWT::encode($token, self::$secret_key);
            $res['error'] = false;
            $res['msj'] = 'Usuario logueado';
            $res['usr'] = $data;
            $res['token'] = $token;
        }

        return $res;
    }

    public function validarSesionApp($token)
    {

        $res = array();
        if ($token == '') {
            $res['error'] = true;
            $res['msj'] = "Token no suministrado";
            echo json_encode($res);
            die();
        } else {
            try {
                $decode = JWT::decode(
                    $token,
                    self::$secret_key,
                    self::$encrypt
                );
            } catch (Exception $e) {
                $error = true;
                $decode = $e->getMessage();
            }

            if (isset($error)) {
                $res['error'] = true;
                $res['msj'] = "Token invalido";
                echo json_encode($res);
                //echo "Token invalido";
                die();
            } else {

                $usuarioid = $decode->data->usuarioid;
                $sql = "SELECT usuarioid nombres, apellidos FROM Ub_usuarios where usuarioid = $usuarioid  and estadoid = 1";
                $query = $this->db->query($sql);
                $data = $query->fetch();
                if (!$data) {
                    $res['error'] = true;
                    $res['msj'] = "Usuario inactivo";
                    echo json_encode($res);
                    die();
                } else {
                    //$res = $decode->data;
                    $res = $data;
                }
            }
        }
        return $res;
    }

    public static function GetData($token)
    {
        return JWT::decode(
            $token,
            self::$secret_key,
            self::$encrypt
        )->data;
    }

    private static function Aud()
    {
        $aud = '';

        if (!empty($_SERVER['HTTP_CLIENT_IP'])) {
            $aud = $_SERVER['HTTP_CLIENT_IP'];
        } elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR'])) {
            $aud = $_SERVER['HTTP_X_FORWARDED_FOR'];
        } else {
            $aud = $_SERVER['REMOTE_ADDR'];
        }

        $aud .= @$_SERVER['HTTP_USER_AGENT'];
        $aud .= gethostname();

        return sha1($aud);
    }

    // Sesion web
    public function loginWeb($usuario, $contrasena)
    {
        $sql = "SELECT usuarioid FROM Ub_usuarios where nombres = '$usuario' and contrasena = '$contrasena' and estadoid = 1";
        $query = $this->db->query($sql);
        $data = $query->fetch();
        
        if ($data) {
            @session_start();
            $_SESSION['session_uber'] = $data->usuarioid;
            return true;
        } {
            return false;
        }
    }

    public function SesionWeb()
    {
        @session_start();
        if (isset($_SESSION['session_uber'])) {

            $sql = "SELECT * FROM Ub_usuarios where usuarioid = ". $_SESSION['session_uber'] . " AND estadoid = 1";
            $query = $this->db->query($sql);
            $data = $query->fetch();
            if ($data) {
                unset($data->contrasena);
                @session_start();
                $_SESSION['session_uber'] = $data->usuarioid;
                return $data;
            } {
                session_destroy();
                session_unset();
                if (!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest') {

                    $res = array();
                    $res['error'] = true;
                    $res['msj'] = 'Su sesión ha expirado';
                    $res['tipo'] = 'error';
                    echo json_encode($res);
                    die();
                } else {
                    header('location: ' . URL . 'login');
                }
            }
        } else {
            session_destroy();
            session_unset();
            if (!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest') {

                $res = array();
                $res['error'] = true;
                $res['msj'] = 'Su sesión ha expirado';
                $res['tipo'] = 'error';
                echo json_encode($res);
                die();
            } else {
                header('location: ' . URL . 'login');
            }
        }
    }

    public function destruirSesionWeb()
    {
        session_destroy();
        session_unset();
        header('location: ' . URL . 'login');
    }
}
