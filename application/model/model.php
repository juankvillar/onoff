<?php

class Model
{
    /**
     * @param object $db A PDO database connection
     */
    function __construct($db)
    {
        try {
            $this->db = $db;
        } catch (PDOException $e) {
            exit('Database connection could not be established.');
        }
    }
    
    public function viewFile($ruta_file, $file)
    {   
        $info = new SplFileInfo($file);
        if($info->getExtension() == 'pdf'){
            header("Content-type: application/pdf");
            header("Content-Disposition: inline; filename=$file");
            readfile("../public/dist/pdf/$ruta_file".$file);    
        }else if($info->getExtension() == 'png' or $info->getExtension() == 'jpg' or $info->getExtension() == 'jpeg'){
            echo "<img src='../../public/dist/img/$ruta_file'>";   
        }else{
            header("Cache-Control: public");
            header("Content-Description: File Transfer");
            header("Content-Disposition: attachment; filename=$ruta_file".$file);
            header("Content-Type: application/zip");
            header("Content-Transfer-Encoding: binary");
            
            // Read the file
            readfile($ruta_file.$file);
            exit;
        }
        
    }

    public function renombrar_y_subir_archivo($ruta, $tmp_name, $name, $iniciales='archivo', $archivo_anterior = '' )
    {


                    $archivo        =   $tmp_name; // capturamos el archivo temporal
                    $nombre_archivo =   $name; // capturamos el nombre del archivo temporal
                    $extencion      =   strtolower(substr( $nombre_archivo, -5 ) ); // Capruramos los 6 ultimos caracteres del nombre dle archivo y lo convertimos a minuscula
                    $array_ext      =   explode('.', $extencion); // partimos el nombre del archivo por el punto y armamos un array
                    $extencion      =   '.'.$array_ext[1]; // optenemos la extencion final o formato dle archivo
                    $hoy            =   getdate(); /*Funcion que captura fecha y hora en un array*/
                    $fecha          =   $hoy['year'].$hoy['mon'].$hoy['mday'].$hoy['hours'].$hoy['minutes'].$hoy['seconds'];
                    $nombre_final   =   $iniciales.'_'.$fecha.$extencion;


                    if ( !file_exists($ruta)) {
                        echo "La ruta para subir el archivo no existe";
                        return false;
                    }

                    if ( copy ( $archivo, $ruta.$nombre_final ) ) {

                            $accion = $nombre_final;

                            if ( file_exists( $ruta.$archivo_anterior ) and $archivo_anterior != '' ) {

                                $respuesta['existe'] = "existe";
                                if( unlink( $ruta.$archivo_anterior ) ){
                                    $respuesta['borrado'] = "borrado";
                                }else{
                                    echo "no borrado";
                                }

                            }else{
                                //echo "no existe";
                            }

                        }else{

                            echo "no copiado";
                            $accion = false;
                        }
                    return $accion;
    }

    public function getEstados(){
        $sql = "SELECT * FROM dl_estados;";
        $query = $this->db->prepare($sql);
        $query->execute();
        return $query->fetchAll();
      }

      public function getCiudades(){
        $sql = "SELECT * FROM dl_ciudades order by ciudad asc;";
        $query = $this->db->prepare($sql);
        $query->execute();
        return $query->fetchAll();
      }

      public function getTipoIdent(){
        $sql = "SELECT * FROM dl_tiposidentificaciones where estadoid = 1 order by tipoidentificacionid asc;";
        $query = $this->db->query($sql);
        return $query->fetchAll();
      }

      public function getMenu(){
        $sql = "SELECT * FROM Ub_menu;";
        $query = $this->db->query($sql);
        return $query->fetchAll();
      }

}
