<?php

class ModelUsuarios
{

    /**
     * @param object $db A PDO database connection
     */
    function __construct($db)
    {
        try {
            $this->db = $db;
        } catch (PDOException $e) {
            exit('Database connection could not be established.');
        }
    }

    public function usrRegistrarme($p)
    {
        extract($p);

        $queryIns = "INSERT INTO Ub_usuarios
                                (
                                nombres,
                                contrasena,
                                fotografia,
                                telefono,
                                conductor,
                                placa,
                                estadoid
                                )
                                VALUES
                                (
                                '$nombres',
                                '$contrasena',
                                'default.png',
                                '$telefono',
                                '$conductor',
                                '$placa',
                                '1');";
        $resultSet_usr = $this->db->exec($queryIns);

        $respuesta = array();
        if ($resultSet_usr) {
            $respuesta['error'] = false;
            $respuesta['msj'] = 'Usuario registrado correctamente';
        } else {
            $respuesta['error'] = true;
            $respuesta['msj'] = 'No se pudo registrar el usuario';
        }
        return $respuesta;
    }

    public function pedirServicio($p)
    {
        extract($p);

        $ubicacion_usuario = $latitude.' '.$longitude;
        $query = "INSERT INTO Ub_servicios
                                (
                                id_usuario,
                                ubicacion_usuario,
                                direccion,
                                stadoid
                                )
                                VALUES
                                (
                                '$id_usuario',
                                '$ubicacion_usuario',
                                '$direccion',
                                '1'
                                );";
        $resultSet_usr = $this->db->exec($query);
        $respuesta = array();
        if ($resultSet_usr) {
            $respuesta['error'] = false;
            $respuesta['msj'] = 'Servicio solicitado correctamente';
        } else {
            $respuesta['error'] = true;
            $respuesta['msj'] = 'No se pudo solicitar el servicio';
        }
        return $respuesta;
    }

    public function servicioPendiente($usuarioid)
    {
        $query = "SELECT * FROM Ub_servicios where id_usuario = $usuarioid and stadoid in(1, 2);";
        $res = $this->db->query($query);
        $respuesta = $res->fetchAll();
        return count($respuesta);
    }

    public function servicioTomado($usuarioid)
    {
        $query = "SELECT s.*, u.nombres FROM Ub_servicios s
                            inner join Ub_usuarios u
                                on s.id_conductor = u.usuarioid
                    where id_usuario = $usuarioid and stadoid = 2;";
        $res = $this->db->query($query);
        $respuesta = $res->fetchAll();

        $res = array();
        $res['servicio'] = count($respuesta);
        $res['det_servicio'] = count($respuesta) == 0 ? array() : $respuesta[0];
        return $res;
    }

    public function cancelarServicio($id_usuario)
    {
        $query = "UPDATE Ub_servicios SET stadoid = '3' WHERE id_usuario = '$id_usuario';";
        $resultSet_usr = $this->db->exec($query);
        $respuesta = array();
        if ($resultSet_usr) {
            $respuesta['error'] = false;
            $respuesta['msj'] = 'Servicio cancelado correctamente';
        } else {
            $respuesta['error'] = true;
            $respuesta['msj'] = 'No se pudo cancelar el servicio';
        }
        return $respuesta;
    }

    public function buscar_servicio($usuarioid)
    {
        $query = "SELECT * FROM Ub_servicios where stadoid = 1 limit 1;";
        $res = $this->db->query($query);
        $respuesta = $res->fetchAll();

        $res = array();
        $res['servicio'] = count($respuesta);
        $res['det_servicio'] = count($respuesta) == 0 ? array() : $respuesta[0];
        return $res;
    }

    public function tomarServicio( $id_conductor )
    {
        $query = "UPDATE Ub_servicios SET id_conductor = '$id_conductor', stadoid = 2 WHERE idservicios = '1';";
        $resultSet_usr = $this->db->exec($query);
        $respuesta = array();
        if ($resultSet_usr) {
            $respuesta['error'] = false;
            $respuesta['msj'] = 'Servicio tomado correctamente';
        } else {
            $respuesta['error'] = true;
            $respuesta['msj'] = 'No se pudo tomar el servicio';
        }
        return $respuesta;
    }
    
}
