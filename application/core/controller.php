<?php

class Controller
{
    /**
     * @var null Database Connection
     */
    public $db = null;
    public $sincache = null;
    public $REQUEST_METHOD = null;
    /**
     * @var null Model
     */
    public $model               = null;
    public $modelSesion         = null;
    public $modelUsuarios       = null;

    /**
     * Whenever controller is created, open a database connection too and load "the model".
     */
    function __construct()
    {
        $this->openDatabaseConnection();
        $this->loadModel();
        $this->sincache = md5(time());
    }

    /**
     * Open the database connection with the credentials from application/config/config.php
     */
    private function openDatabaseConnection()
    {
        // set the (optional) options of the PDO connection. in this case, we set the fetch mode to
        // "objects", which means all results will be objects, like this: $result->user_name !
        // For example, fetch mode FETCH_ASSOC would return results like this: $result["user_name] !
        // @see http://www.php.net/manual/en/pdostatement.fetch.php
        $options = array(PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_OBJ, PDO::ATTR_ERRMODE => PDO::ERRMODE_WARNING);

        // generate a database connection, using the PDO connector
        // @see http://net.tutsplus.com/tutorials/php/why-you-should-be-using-phps-pdo-for-database-access/
        $this->db = new PDO(DB_TYPE . ':host=' . DB_HOST . ';dbname=' . DB_NAME . ';charset=' . DB_CHARSET, DB_USER, DB_PASS, $options);
    }

    public function getParams($method)
    {   
        if($_SERVER['REQUEST_METHOD'] == $method){
            switch ($_SERVER['REQUEST_METHOD']) {
                case 'GET':
                    $params = $_GET;
                    return $params;
                break;
                case 'POST':
                    foreach ($_REQUEST as $key => $value) {
                        if( is_array($value) ) {continue;}
                        $_POST[$key] = trim(filter_var ($value, FILTER_SANITIZE_STRING));
                    }
                    $params = $_POST;
                    return $params;
                break;
                case 'DELETE':
                    parse_str(file_get_contents('php://input'), $params);
                    return $params;
                break;
                default:
                        return null;
                        break;
            }

        }else{
            echo "Metodo no permitido";
            die();
        }
    }

    /**
     * Loads the "model".
     * @return object model
     */
    public function loadModel()
    {
        require APP . 'model/model.php';
        require APP . 'model/model_sesion.php';
        require APP . 'model/model_usuarios.php';

        // create new "model" (and pass the database connection)
        $this->model                = new Model($this->db);
        $this->modelSesion          = new ModelSesion($this->db);
        $this->modelUsuarios        = new ModelUsuarios($this->db);
    }
}
