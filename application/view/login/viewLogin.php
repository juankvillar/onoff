<!DOCTYPE html>
<html>

<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Login Uber</title>
  <!-- Tell the browser to be responsive to screen width -->
  <link rel="icon" type="image/x-icon" href="<?php echo URL ?>public/template/dist/img/logo.ico">
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.7 -->
  <link rel="stylesheet" href="<?php echo URL ?>public/template/bower_components/bootstrap/dist/css/bootstrap.min.css">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="<?php echo URL ?>public/template/bower_components/font-awesome/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="<?php echo URL ?>public/template/bower_components/Ionicons/css/ionicons.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="<?php echo URL ?>public/template/dist/css/AdminLTE.min.css">
  <!-- Material Design -->
  <link rel="stylesheet" href="<?php echo URL ?>public/template/dist/css/bootstrap-material-design.min.css">
  <link rel="stylesheet" href="<?php echo URL ?>public/template/dist/css/ripples.min.css">
  <link rel="stylesheet" href="<?php echo URL ?>public/template/dist/css/MaterialAdminLTE.min.css">
  <!-- iCheck -->
  <!-- <link rel="stylesheet" href="<?php echo URL ?>public/template/plugins/iCheck/square/blue.css"> -->

  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->

  <!-- Google Font -->
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
</head>
<style>
  .login-box-body,
  .register-box-body {
    background: rgb(255, 255, 255, 0.1);
    border-radius: 5%;
  }

  ::placeholder {
    /* Firefox, Chrome, Opera */
    color: gray !important;
  }

  .login-box-body .form-control-feedback,
  .register-box-body .form-control-feedback {
    color: gray;
  }

  .form-control {
    color: white;
  }

  a.registrame:hover {
    text-decoration: underline;
  }
</style>

<body class="hold-transition login-page" style="overflow:hidden; background-color: black">
  <div class="login-box" id="box_login">
    <div class="login-logo">
      <img style="width: 70%;" src="<?php echo URL ?>public/template/dist/img/logo.png" alt="">
    </div>
    <!-- /.login-logo -->
    <div class="login-box-body">
      <!-- <p class="login-box-msg">Sign in to start your session</p> -->

      <form method="post" action="<?php echo URL; ?>login/loguearse/aut?=<?php echo md5(time());  ?> " autocomplete="off">
        <div class="form-group has-feedback">
          <input type="text" class="form-control" id="usuario" name="usuario" placeholder="Usuario" autocomplete="off">
          <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
        </div>
        <div class="form-group has-feedback">
          <input type="password" class="form-control" id="contrasena" name="contrasena" placeholder="Contraseña" autocomplete="off">
          <span class="glyphicon glyphicon-lock form-control-feedback"></span>
        </div>
        <div class="row">
          <div class="col-xs-7">
            <div class="checkbox">
              <label>
                <a class="registrame log_reg" href="#" style="color: white;">Registrarme</a>
              </label>
            </div>
          </div>
          <!-- /.col -->
          <div class="col-xs-5">
            <button type="submit" class="btn bg-black">Ingresar</button>
          </div>
          <!-- /.col -->
        </div>
      </form>
      <!-- /.social-auth-links -->
    </div>
    <!-- /.login-box-body -->
  </div>
  <!-- /.login-box -->

  <div class="login-box" id="box_registrarme" style="display: none;">
    <div class="login-logo">
      <img style="width: 70%;" src="<?php echo URL ?>public/template/dist/img/logo.png" alt="">
    </div>
    <!-- /.login-logo -->
    <div class="login-box-body">
      <!-- <p class="login-box-msg">Sign in to start your session</p> -->

      <form method="post" id="frm_registrarme">
        <div class="form-group has-feedback">
          <input type="text" class="form-control" id="nombres" name="nombres" placeholder="Nombre" autocomplete="off" required>
          <span class="glyphicon glyphicon-user form-control-feedback"></span>
        </div>
        <div class="form-group has-feedback">
          <input type="password" class="form-control" id="contrasena" name="contrasena" placeholder="Contraseña" autocomplete="off" required>
          <span class="glyphicon glyphicon-lock form-control-feedback"></span>
        </div>
        <div class="form-group has-feedback">
          <input type="text" class="form-control" id="telefono" name="telefono" placeholder="Teléfono" autocomplete="off" required>
          <span class="glyphicon glyphicon-earphone form-control-feedback"></span>
        </div>
        <div class="form-group has-feedback">
          <select name="conductor" id="conductor" class="form-control" placeholder="Conductor" required>
            <option value="">Es un conductor ?</option>
            <option style="color: black;" value="1">Si</option>
            <option style="color: black;" value="2">No</option>
          </select>
          <span class="glyphicon glyphicon-off form-control-feedback"></span>
        </div>
        <div class="form-group has-feedback" style="display: none;" id="txtplaca">
          <input type="text" class="form-control" id="placa" name="placa" placeholder="Placa vehiculo" autocomplete="off">
          <span class="glyphicon glyphicon-tag form-control-feedback"></span>
        </div>
        <div class="row">
          <div class="col-xs-6">
            <div class="checkbox">
              <label>
                <a class="registrame log_reg" href="#" style="color: white;">Volver</a>
              </label>
            </div>
          </div>
          <!-- /.col -->
          <div class="col-xs-6">
            <button type="submit" class="btn bg-black">Registrarme</button>
          </div>
          <!-- /.col -->
        </div>
      </form>
      <!-- /.social-auth-links -->
    </div>
    <!-- /.login-box-body -->
  </div>
  <!-- /.login-box -->

  <!-- jQuery 3 -->
  <script src="<?php echo URL ?>public/template/bower_components/jquery/dist/jquery.min.js"></script>
  <!-- Bootstrap 3.3.7 -->
  <script src="<?php echo URL ?>public/template/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
  <!-- Material Design -->
  <script src="<?php echo URL ?>public/template/dist/js/material.min.js"></script>
  <script src="<?php echo URL ?>public/template/dist/js/ripples.min.js"></script>
  <script>
    var url = "<?php echo URL; ?>";
  </script>
  <script>
    
    $.material.init();
    $('.log_reg').click(function(e) {
      e.preventDefault();
      $('#box_login').toggle();
      $('#box_registrarme').toggle();
    });

    $('#conductor').change(function(e) {
      e.preventDefault();
      if ($(this).val() == 1) {
        $('#txtplaca').css("display", "block");
      } else {
        $('#txtplaca').css("display", "none");
      }
    });

    $('#frm_registrarme').submit(function(e) {
      e.preventDefault();
      if ($('#conductor').val() == 1 && $('#placa').val().trim() == "") {
        alert("Por favor llena el campo de placa"); return false;
      }

      let datos = $( this ).serializeArray();
      $.ajax({
          type: "post",
          url: url + "usuarios/registrarme",
          dataType: "json",
          data: datos,
        }).done(function(data) {
          $('#frm_registrarme')[0].reset();
          alert(data.msj)
          $('#box_login').toggle();
          $('#box_registrarme').toggle();

        })
        .fail(function(data) {
          console.log("error");
        })
    });
  </script>
</body>

</html>