<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1>
      Usuario
    </h1>
    <div class="row">
      <div class="col-md-3"><input id="direccion" type="text" class="form-control" placeholder="Dirección donde vas"></div>
      <div class="col-md-3">
        <?php if ( $servicio == 0 ) {  ?>
          <button id="btn_servicio" type="button" class="btn btn-primary bg-green">Pedir servicio</button>
          <button id="btn_cancelar_servicio" style="display: none;" type="button" class="btn btn-primary bg-red">cancelar servicio</button>
        <?php } else { ?>
          <button id="btn_servicio" style="display: none;" type="button" class="btn btn-primary bg-green">Pedir servicio</button>
          <button id="btn_cancelar_servicio" type="button" class="btn btn-primary bg-red">cancelar servicio</button>
        <?php } ?>
        <strong><div class="text-green" id="conductor"></div></strong>
        

      </div>
    </div>
  </section>


</div>
<!-- /.content-wrapper -->