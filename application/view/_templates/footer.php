<footer class="main-footer">
    <div class="pull-right hidden-xs">
      <b>Version</b> 1.0
    </div>
    <strong>Copyright &copy; 2021 <a href="#" style="color: black;">Uber</a></strong> Derechos reservados
    reserved.
  </footer>
</div>
<!-- ./wrapper -->

<script>
    var url = "<?php echo URL; ?>";
</script>
<!-- jQuery 3 -->
<script src="<?php echo URL ?>public/template/bower_components/jquery/dist/jquery.min.js"></script>
<!-- Bootstrap 3.3.7 -->
<script src="<?php echo URL ?>public/template/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
<!-- Select2 -->
<script src="<?php echo URL ?>public/template/bower_components/select2/dist/js/select2.full.min.js"></script>
<!-- Material Design -->
<script src="<?php echo URL ?>public/template/dist/js/material.min.js"></script>
<script src="<?php echo URL ?>public/template/dist/js/ripples.min.js"></script>
<script>
    $.material.init();
</script>
<script src="<?php echo URL ?>public/template/bower_components/PACE/pace.min.js"></script>
<script src="<?php echo URL ?>public/template/bower_components/datatables.net/js/jquery.dataTables.min.js"></script>
<script src="<?php echo URL ?>public/template/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
<!-- SlimScroll -->
<script src="<?php echo URL ?>public/template/bower_components/jquery-slimscroll/jquery.slimscroll.min.js"></script>
<!-- FastClick -->
<script src="<?php echo URL ?>public/template/bower_components/fastclick/lib/fastclick.js"></script>
<!-- AdminLTE App -->
<script src="<?php echo URL ?>public/template/dist/js/adminlte.min.js"></script>
<!-- AdminLTE for demo purposes -->
<script src="<?php echo URL ?>public/template/dist/js/demo.js"></script>

<!-- noty -->
<script src="<?php echo URL ?>public/js/noty/noty.js"></script>
<script src="<?php echo URL ?>public/js/mijs.js"></script>
<script>
  function noty( mensaje, type = 'success' ){

    if(typeof(mensaje) != "undefined"){ 
      if(mensaje == 'Su sesión ha expirado'){
              setTimeout(function(){ location.reload()  }, 2000); 
            }
            
            $('.noty_bar').click() // oculta una animación si está abierta
            new Noty({
              text: '<strong>Atención</strong><br><div class="text-left">'+mensaje+'</div>',
              type: type,
              theme: 'metroui',
              layout: 'topRight',
              timeout: 4000
            }).show();
          }
					
        }
        
        $(document).ajaxStart(function () {
          Pace.restart()
          $( ".btn" ).prop( "disabled", true );
          $('.btn').prepend('<i class="fa fa-refresh fa-spin"></i> ');
        })
        $(document).ajaxComplete(function () {
          $( ".btn" ).prop( "disabled", false );
          $('.fa-refresh.fa-spin').replaceWith('');
        })

</script>

</body>
</html>