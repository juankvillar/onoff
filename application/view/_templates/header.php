<?php //$usuario = $this->modelSesion->SesionWeb(); 
?>
<!DOCTYPE html>
<html lang="es">

<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Uber</title>
  <link rel="icon" type="image/x-icon" href="<?php echo URL ?>public/template/dist/img/logo.ico">
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.7 -->
  <link rel="stylesheet" href="<?php echo URL ?>public/template/bower_components/bootstrap/dist/css/bootstrap.min.css">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="<?php echo URL ?>public/template/bower_components/font-awesome/css/font-awesome.min.css">
  <!-- Select2 -->
  <link rel="stylesheet" href="<?php echo URL ?>public/template/bower_components/select2/dist/css/select2.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="<?php echo URL ?>public/template/bower_components/Ionicons/css/ionicons.min.css">
  <!-- DataTables -->
  <link rel="stylesheet" href="<?php echo URL ?>public/template/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="<?php echo URL ?>public/template/dist/css/AdminLTE.min.css">
  <!-- Material Design -->
  <link rel="stylesheet" href="<?php echo URL ?>public/template/dist/css/bootstrap-material-design.min.css">
  <link rel="stylesheet" href="<?php echo URL ?>public/template/dist/css/ripples.min.css">
  <link rel="stylesheet" href="<?php echo URL ?>public/template/dist/css/MaterialAdminLTE.min.css">
  <!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
  <link rel="stylesheet" href="<?php echo URL ?>public/template/dist/css/skins/all-md-skins.min.css">

  <!-- pace -->
  <link rel="stylesheet" href="<?php echo URL ?>public/template/plugins/pace/pace.min.css">

  <!-- noty -->
  <link rel="stylesheet" href="<?php echo URL ?>public/css/noty/noty.css">

  <!-- noty -->
  <link rel="stylesheet" href="<?php echo URL ?>public/css/summa.css?version='<?php echo md5(time()); ?>'">
  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->

  <!-- Google Font -->
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">

</head>
<style type="text/css" media="screen">
  .nav-tabs-custom>.nav-tabs>li.active {
    border-top-color: #4caf50;
  }

  .nav-tabs-custom>.nav-tabs>li.active>a {
    color: white !important;
  }

  .nav-tabs-custom>.nav-tabs>li>a {
    color: #444 !important;
  }

  @media (min-width: 992px) {
    .modal-lg {
      width: 1200px;
      height: 600px;
      /* control height here */
    }
  }

  .modal img {
    max-width: 25%;
    border-radius: 75px;
    /*border: 1px solid #aaa;*/
  }

  #imagen>p {
    color: #00a65a;
    cursor: pointer;
    font-weight: bold;
  }

  span.btn-xs {
    cursor: pointer !important;
  }

  .skin-green-light .sidebar-menu>li>a {
    border-bottom: 1px solid lightgray;
  }

  @media only screen and (min-device-width : 768px) and (max-device-width : 1024px) {
    .logo-lg>img {
      width: 40% !important;
    }
  }

  @media only screen and (min-device-width : 375px) and (max-device-width : 667px) {
    .logo-lg>img {
      width: 40% !important;
    }
  }

  @media only screen and (min-device-width : 414px) and (max-device-width : 736px) {
    .logo-lg>img {
      width: 40% !important;
    }
  }

  @media only screen and (min-device-width : 320px) and (max-device-width : 568px) {
    .logo-lg>img {
      width: 40% !important;
    }
  }
</style>

<body class="hold-transition skin-green-light sidebar-mini">
  <!-- Site wrapper -->
  <div class="wrapper">

    <header class="main-header">
      <!-- Logo -->
      <a href="<?php echo URL ?>public/" class="logo" style="background: black;">
        <!-- mini logo for sidebar mini 50x50 pixels -->
        <span class="logo-mini"><img style="width: 100%;" src="<?php echo URL ?>public/template/dist/img/logo.png" alt=""></span>
        <!-- logo for regular state and mobile devices -->
        <span class="logo-lg"><img style="width: 50%;" src="<?php echo URL ?>public/template/dist/img/logo.png" alt=""></span>
        <!-- <span class="logo-mini" style="color: #222d32;font-size: 60%;">SUMMA</span>
      <span class="logo-lg" style="color: #222d32;">SUMMA</span> -->
      </a>
      <!-- Header Navbar: style can be found in header.less -->
      <nav class="navbar navbar-static-top">
        <!-- Sidebar toggle button-->
        <a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button">
          <span class="sr-only">Toggle navigation</span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
        </a>

        <div class="navbar-custom-menu">
          <ul class="nav navbar-nav">
            <!-- Messages: style can be found in dropdown.less-->
            <!-- Notifications: style can be found in dropdown.less -->
            <!-- Tasks: style can be found in dropdown.less -->

            <!-- User Account: style can be found in dropdown.less -->
            <li class="dropdown user user-menu">
              <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                <img src="<?php echo URL ?>public/dist/img/usuarios/<?php echo $this->modelSesion->SesionWeb()->fotografia ?>" class="user-image" alt="User Image">
                <span class="hidden-xs"><?php echo $this->modelSesion->SesionWeb()->nombres ?></span>
              </a>
              <ul class="dropdown-menu">
                <!-- Menu Footer-->
                <li class="user-footer">
                  <div class="pull-left">
                    <a href="<?php echo URL ?>perfil/" class="btn btn-default btn-flat">Perfil</a>
                  </div>
                  <div class="pull-right">
                    <a href="<?php echo URL ?>login/salir" class="btn btn-default btn-flat">Salir</a>
                  </div>
                </li>
              </ul>
            </li>
            <!-- Control Sidebar Toggle Button -->
          </ul>
        </div>
      </nav>
    </header>

    <!-- =============================================== -->
  <style>
    @media (min-width: 992px) {
    .ocultar {
      display: none;
    }
  }
  </style>
    <!-- Left side column. contains the sidebar -->
    <aside class="main-sidebar">
      <!-- sidebar: style can be found in sidebar.less -->
      <section class="sidebar">
        <!-- search form -->
      <br class="ocultar">
      <br class="ocultar">
      <br class="ocultar">
        <!-- <form action="#" method="get" class="sidebar-form">
        <div class="input-group">
          <input type="text" name="q" class="form-control" placeholder="Buscar...">
          <span class="input-group-btn">
                <button type="submit" name="search" id="search-btn" class="btn btn-flat"><i class="fa fa-search"></i>
                </button>
              </span>
        </div>
      </form> -->
        <!-- /.search form -->
        <!-- sidebar menu: : style can be found in sidebar.less -->
        <ul class="sidebar-menu" data-widget="tree">
          <?php foreach ($this->model->getMenu() as $key => $value) {
            if ($this->modelSesion->SesionWeb()->conductor == $value->menuid) { ?>
              <li><a href="<?php echo $value->vista == '#' ? '#' : URL.$value->vista; ?>"><i class="fa <?php echo $value->icono ?> "></i> <span> <?php echo $value->menu ?></span></a></li>
              <?php } ?>          
          <?php } ?>
          <li><a href="<?php echo URL ?>login/salir"><i class="fa fa-power-off"></i> <span> Salir</span></a></li>
        </ul>
      </section>
      <!-- /.sidebar -->
    </aside>
    <br class="ocultar">
    <!-- =============================================== -->