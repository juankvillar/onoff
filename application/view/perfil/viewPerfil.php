<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1>Mi perfil</h1>
  </section>

  <section class="content">

    <div class="row">
      <div class="col-md-3">

        <!-- Profile Image -->
        <div class="box box-primary">
          <div class="box-body box-profile">
            <img class="profile-user-img img-responsive img-circle" src="<?php echo URL ?>public/dist/img/usuarios/<?php echo $usuarios->fotografia ?>" alt="User profile picture">

            <h3 class="profile-username text-center"><?php echo $usuarios->nombres ?></h3>

            <p class="text-muted text-center"><?php echo $usuarios->perfil ?></p>

            <ul class="list-group list-group-unbordered">
              <li class="list-group-item">
                <b>Ingreso:</b> <a class="pull-right"><?php echo $usuarios->fecha_ingreso ?></a>
              </li>
              <li class="list-group-item">
                <b>Empresa:</b> <a class="pull-right"><?php echo $usuarios->empresa ?></a>
              </li>
              <li class="list-group-item">
                <b>Horas de trabajo:</b> <a class="pull-right">11.245</a>
              </li>
            </ul>
          </div>
          <!-- /.box-body -->
        </div>
        <!-- /.box -->

        <!-- About Me Box -->
        <div class="box box-primary">
          <div class="box-header with-border">
            <h3 class="box-title">Información</h3>
          </div>
          <!-- /.box-header -->
          <div class="box-body">
            <strong><i class="fa fa-map-marker margin-r-5"></i> Contacto</strong>

            <p class="text-muted">
              Dirección: <?php echo $usuarios->direccion ?>
              <br>
              Teléfono: <?php echo $usuarios->telefono ?>
              <br>
              Celular: <?php echo $usuarios->celular ?>
            </p>
            <hr>
          </div>
          <!-- /.box-body -->
        </div>
        <!-- /.box -->
      </div>
      <!-- /.col -->
      <div class="col-md-9">
        <div class="nav-tabs-custom">
          <ul class="nav nav-tabs">
            <li class="active"><a href="#datos" data-toggle="tab"><i class="fa fa-briefcase"></i> Datos</a></li>
            <li><a href="#contrasena" data-toggle="tab"><i class="fa fa-key"></i> Contraseña</a></li>
          </ul>
          <div class="tab-content">
            <div class="active tab-pane" id="datos">
              <form autocomplete="off" id="frm_perfil">
                <div class="row">
                  <div class="col-md-6">
                    <div class="form-group">
                      <label for="nombres">Nombres</label>
                      <input type="text" class="form-control" id="nombres" name="nombres" placeholder="" autocomplete="off" readonly value="<?php echo $usuarios->nombres ?>">
                    </div>
                  </div>
                  <div class="col-md-6">
                    <div class="form-group">
                      <label for="apellidos">Apellidos</label>
                      <input type="text" class="form-control" id="apellidos" name="apellidos" placeholder="" autocomplete="off" readonly value="<?php echo $usuarios->apellidos ?>">
                    </div>
                  </div>
                </div>
                <div class="row">
                  <div class="col-md-6">
                    <div class="form-group">
                      <label for="tipo_identificacion_id">Tipo identificación</label>
                      <input type="text" class="form-control" id="tipo_identificacion" name="tipo_identificacion" placeholder="" autocomplete="off" readonly value="<?php echo $usuarios->ti_identificacion ?>">
                    </div>
                  </div>
                  <div class="col-md-6">
                    <div class="form-group">
                      <label for="identificacion">Nº Identificación</label>
                      <input type="number" class="form-control" id="identificacion" name="identificacion" placeholder="" autocomplete="off" readonly value="<?php echo $usuarios->identificacion ?>">
                    </div>
                  </div>
                </div>
                <div class="row">
                  <div class="col-md-6">
                    <div class="form-group">
                      <label for="ciudad_exp">Lugar de expedición </label>
                      <input type="text" class="form-control" id="ciudad_exp" name="ciudad_exp" placeholder="" autocomplete="off" readonly value="<?php echo $usuarios->ci_expedicion ?>">
                    </div>
                  </div>
                  <div class="col-md-6">
                    <div class="form-group">
                      <label for="fecha_expedicion">Fecha de expedición</label>
                      <input type="text" class="form-control" id="fecha_expedicion" name="fecha_expedicion" placeholder="" autocomplete="off" readonly value="<?php echo $usuarios->fecha_expedicion ?>">
                    </div>
                  </div>
                </div>

                <div class="row">
                  <div class="col-md-6">
                    <div class="form-group">
                      <label for="telefono">Télefono</label>
                      <input type="number" class="form-control" id="telefono" name="telefono" placeholder="" autocomplete="off" readonly value="<?php echo $usuarios->telefono ?>">
                    </div>
                  </div>
                  <div class="col-md-6">
                    <div class="form-group">
                      <label for="celular">Celular</label>
                      <input type="number" class="form-control" id="celular" name="celular" placeholder="" autocomplete="off" readonly value="<?php echo $usuarios->celular ?>">
                    </div>
                  </div>
                </div>

                <div class="row">
                  <div class="col-md-6">
                    <div class="form-group">
                      <label for="email">Email</label>
                      <input type="email" class="form-control" id="email" name="email" placeholder="" autocomplete="off" readonly value="<?php echo $usuarios->email ?>">
                    </div>
                  </div>
                  <div class="col-md-6">
                    <div class="form-group">
                      <label for="tarifa_arl">Tarifa arl</label>
                      <input type="number" class="form-control" id="tarifa_arl" name="tarifa_arl" placeholder="" autocomplete="off" readonly value="<?php echo $usuarios->arl_tarifa ?>">
                    </div>
                  </div>
                </div>

                <!-- row -->
                <div class="row">
                  <div class="col-md-6">
                    <div class="form-group">
                      <label for="direccion">Dirección</label>
                      <input type="text" class="form-control" id="direccion" name="direccion" placeholder="" autocomplete="off" readonly value="<?php echo $usuarios->direccion ?>">
                    </div>
                  </div>
                  <div class="col-md-6">
                    <div class="form-group">
                      <label for="barrio">Barrio</label>
                      <input type="text" class="form-control" id="barrio" name="barrio" placeholder="" autocomplete="off" readonly value="<?php echo $usuarios->barrio ?>">
                    </div>
                  </div>
                </div>
                <!-- ./row -->
              </form>
            </div>
            <!-- /.tab-pane -->
            <div class="tab-pane" id="contrasena">
              <form autocomplete="off" id="frm_contraseña">
                <div class="row">
                  <div class="col-md-6">
                    <div class="form-group">
                      <label for="nombres">Contraseña actual</label>
                      <input type="text" class="form-control" id="pass_old" name="pass_old" placeholder="" autocomplete="off">
                    </div>
                  </div>
                  <div class="col-md-6">
                    <div class="form-group">
                      <label for="apellidos">Nueva contraseña</label>
                      <input type="text" class="form-control" id="pass_new" name="pass_new" placeholder="" autocomplete="off">
                    </div>
                  </div>
                </div>
                <div class="row">
                  <div class="col-md-6">
                    <button type="submit" class="btn bg-green">Cambiar</button>
                  </div>
                </div>
              </form>
            </div>
            <!-- /.tab-pane -->
          </div>
          <!-- /.tab-content -->
        </div>
        <!-- /.nav-tabs-custom -->
      </div>
      <!-- /.col -->
    </div>
    <!-- /.row -->

  </section>
  <!-- /.content -->
</div>
<!-- /.content-wrapper -->