<?php

class Home extends Controller
{

    public function index()
    {
        $this->modelSesion->SesionWeb();
        if ($this->modelSesion->SesionWeb()->conductor == 1) {
            require APP . 'view/_templates/header.php';
            require APP . 'view/home/viewConductor.php';
            require APP . 'view/_templates/footer.php';
            echo '<script src="'.URL.'/public/js/conductor.js?version='.$this->sincache.'"></script>';
        }else{
            $usuarioid = $this->modelSesion->SesionWeb()->usuarioid;
            $servicio = $this->modelUsuarios->servicioPendiente( $usuarioid );
            require APP . 'view/_templates/header.php';
            require APP . 'view/home/index.php';
            require APP . 'view/_templates/footer.php';
            echo '<script src="'.URL.'/public/js/usuarios.js?version='.$this->sincache.'"></script>';
        }
        
    }

}
