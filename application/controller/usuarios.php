<?php
@session_start();
class Usuarios extends Controller
{
    
    // usuario
    public function registrarme(){
        $param = $this->getParams('POST');
        $param['contrasena'] = md5($param['contrasena']);
        $data = $this->modelUsuarios->usrRegistrarme( $param );
        echo json_encode($data);
    }

    public function pedirServicio(){
        $usuarioid = $this->modelSesion->SesionWeb()->usuarioid;
        $param = $this->getParams('POST');
        $param['id_usuario'] = $usuarioid;
        $data = $this->modelUsuarios->pedirServicio( $param );
        echo json_encode($data);
    }

    public function cancelarServicio(){
        $usuarioid = $this->modelSesion->SesionWeb()->usuarioid;
        $param = $this->getParams('POST');
        $data = $this->modelUsuarios->cancelarServicio( $usuarioid );
        echo json_encode($data);
    }
    
    public function servicioTomado(){
        $usuarioid = $this->modelSesion->SesionWeb()->usuarioid;
        $data = $this->modelUsuarios->servicioTomado($usuarioid);
        echo json_encode($data);
    }
    

    // Conductor
    public function buscar_servicio(){
        $usuarioid = $this->modelSesion->SesionWeb()->usuarioid;
        $data = $this->modelUsuarios->buscar_servicio( $usuarioid );
        echo json_encode($data);
    }

    public function tomarServicio(){
        $usuarioid = $this->modelSesion->SesionWeb()->usuarioid;
        $data = $this->modelUsuarios->tomarServicio( $usuarioid );
        echo json_encode($data);
    }
}