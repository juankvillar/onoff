<?php

class Perfil extends Controller
{

    public function index()
    {
        $this->modelSesion->SesionWeb();
        $this->getParams('GET');
        $usuarios = $this->modelSesion->SesionWeb();
        $archivos = $this->modelUsuarios->getUsuario( $usuarios->usuarioid );
        require APP . 'view/_templates/header.php';
        require APP . 'view/perfil/viewPerfil.php';
        require APP . 'view/_templates/footer.php';
        echo '<script src="'.URL.'/public/js/perfil.js?version='.$this->sincache.'"></script>';
    }

    public function cambiarPass()
    {   
        $this->modelSesion->SesionWeb();
        $param = $this->getParams('POST');
        $usuarios = $this->modelSesion->SesionWeb();
        $param['usuarioid'] = $usuarios->usuarioid;
        $json = $this->modelUsuarios->cambiarPass( $param );
        echo json_encode($json);
    }

    
}
