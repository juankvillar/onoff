<?php

class Login extends Controller
{

    public function index(){
        @session_start();
        if(isset($_SESSION['session_uber'])){
            header('location: ' . URL . '');
        }
        require APP . 'view/login/viewLogin.php';
    }

    public function loguearse(){
        $param = $this->getParams('POST');
        $param['contrasena'] = md5($param['contrasena']);
        $login = $this->modelSesion->loginWeb($param['usuario'], $param['contrasena']);
        if( $login ){
            header('location: ' . URL . '');
        }else{
            @session_start();
            header('location: ' . URL . 'login');
        }
    }

    public function salir()
    {   
        $usuarioid = $this->modelSesion->SesionWeb()->usuarioid;
        $this->modelSesion->destruirSesionWeb( $usuarioid );
    }
}
