let latitude = '';
let longitude = '';

navigator.geolocation.getCurrentPosition(function(position) {
  console.log(position.coords.latitude, position.coords.longitude);
  latitude = position.coords.latitude;
  longitude = position.coords.longitude;
});

$('#btn_servicio').click(function (e) { 
  e.preventDefault();

  if ( latitude == ''){ alert("Estamos cargando tu ubicación, por favor valida que tengas este permiso activo"); return false; }
  
  if ( confirm('¿Está seguro de solicitar un servicio?') ) {
    $.ajax({
      type: "post",
      url: url + "usuarios/pedirServicio",
      dataType: "json",
      data: {direccion: $("#direccion").val(), latitude: latitude, longitude:longitude },
    }).done(function(data) {
      alert(data.msj)
      $('#btn_cancelar_servicio').css('display', 'block');
      $('#btn_servicio').css('display', 'none');
    })
    .fail(function(data) {
      console.log("error");
    })  
  }
});

$('#btn_cancelar_servicio').click(function (e) { 
  e.preventDefault();
  
  if ( confirm('¿Está seguro de calcelar el servicio?') ) {
    $.ajax({
      type: "post",
      url: url + "usuarios/cancelarServicio",
      dataType: "json"
    }).done(function(data) {
      alert(data.msj)
      $('#btn_cancelar_servicio').css('display', 'none');
      $('#conductor').css('display', 'none');
      $('#btn_servicio').css('display', 'block');
    })
    .fail(function(data) {
      console.log("error");
    })  
  }
});

$('#frm_registrarme').submit(function(e) {
  e.preventDefault();
});


function servicioTomado() {
  $.ajax({
    type: "post",
    url: url + "usuarios/servicioTomado",
    dataType: "json",
  })
    .done(function (data) {
      if (data.servicio == 1) {
        $('#direccion').val(data.det_servicio.direccion);
        $('#conductor').html('Servicio tomdo por '+data.det_servicio.nombres);
      }
        
    })
    .fail(function (data) {
      console.log("error");
    });
}

setInterval(function () {
  servicioTomado();
}, 4000);