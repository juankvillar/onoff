let idservicios = '';
$("#btn_servicio").click(function (e) {
  e.preventDefault();

  if (confirm("¿Está seguro de tomar el servicio?")) {
    $.ajax({
      type: "post",
      url: url + "usuarios/tomarServicio",
      dataType: "json",
      data: {idservicios:idservicios},
    })
      .done(function (data) {
        alert(data.msj);
        $("#btn_cancelar_servicio").css("display", "block");
        $("#btn_servicio").css("display", "none");
      })
      .fail(function (data) {
        console.log("error");
      });
  }
});

function buscar_servicio() {
  $.ajax({
    type: "post",
    url: url + "usuarios/buscar_servicio",
    dataType: "json",
  })
    .done(function (data) {
      console.log(data.servicio)
        if( data.servicio == 1 ){
          $('#notificacion').css("display", "block");
          $('#mensaje').html(data.det_servicio.direccion);
          idservicios = data.det_servicio.idservicios
        }else{
          $('#notificacion').css("display", "none");
        }
    })
    .fail(function (data) {
      console.log("error");
    });
}

setInterval(function () {
  buscar_servicio();
}, 4000);
