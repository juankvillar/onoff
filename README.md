## Instalación

1. Crear una base de datos con el nombre BD_Uber, o el que usted desee, editar la conexión a la base de datos en el archivo, `application/config/config.php` poner el nomre de la base de datos que usted eligió
2. Ejecute o importe el archivo .sql, dentro de la carpeta `_install/`
3. Copie la el contenido de la carpeta en su servidor y listo.